#!/bin/bash
set -e

mkdir -p ~/.local/bin/

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -m 0755 kubectl ~/.local/bin/kubectl


curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind
mv ./kind ~/.local/bin/kind

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | USE_SUDO=false HELM_INSTALL_DIR=~/.local/bin  bash

rm -rf istio-*/ || true
curl -L https://istio.io/downloadIstio | sh -
mv istio-*/ istio/
mv istio/bin/istioctl ~/.local/bin/istioctl


export PATH=$HOME/.local/bin:$PATH


kind create cluster --config=single-node.yaml || true
# Calico
curl https://docs.projectcalico.org/manifests/calico.yaml | kubectl apply -f -
# CoreDNS
kubectl scale deployment --replicas 1 coredns --namespace kube-system
# Metrics Server
helm repo add stable https://charts.helm.sh/stable --force-update
helm repo update
helm upgrade metrics-server --install --set "args={--kubelet-insecure-tls, --kubelet-preferred-address-types=InternalIP}" stable/metrics-server --namespace kube-system

istioctl install -y -f istio.yaml

kubectl apply -f istio/samples/addons/grafana.yaml
kubectl apply -f istio/samples/addons/kiali.yaml
kubectl apply -f istio/samples/addons/prometheus.yaml
kubectl apply -f istio/samples/addons/jaeger.yaml